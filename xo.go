package main

import (
	"encoding/json"
	"fmt"
	"html"
	"html/template"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

const FIELD_SIZE = 16
const VERTICAL = 0
const HORIZONTAL = 1
const SLASH = 2
const BACKSLASH = 3

type GameValue struct {
	field  [FIELD_SIZE * FIELD_SIZE][4][6]int
	winPos [5]int
}

var Index *template.Template

func dbg(w io.Writer, writePre bool, vars ...interface{}) {
	if writePre {
		io.WriteString(w, "<pre>\n")
	}
	for i, v := range vars {
		fmt.Fprintf(w, "» item %d type %T:\n", i, v)
		j, err := json.MarshalIndent(v, "", "    ")
		switch {
		case err != nil:
			fmt.Fprintf(w, "error: %v", err)
		case len(j) < 3: // {}, empty struct maybe or empty string, usually mean unexported struct fields
			w.Write([]byte(html.EscapeString(fmt.Sprintf("%+v", v))))
		default:
			w.Write(j)
		}
		w.Write([]byte("\n\n"))
	}
	if writePre {
		io.WriteString(w, "</pre>\n")
	}
}

func arrayMax(a [FIELD_SIZE * FIELD_SIZE]int) int {
	max := a[0]
	for _, v := range a {
		if v > max {
			max = v
		}
	}
	return max
}

func findPositions(a [FIELD_SIZE * FIELD_SIZE]int, val int) []int {
	pos := []int{}
	for k, v := range a {
		if v == val {
			pos = append(pos, k)
		}
	}
	return pos
}

func replaceAtIndex(in string, r byte, i int) string {
	out := []byte(in)
	out[i] = r
	return string(out)
}

func loadTemplate() {
	var err error
	Index, err = template.ParseFiles("index.html")
	if err != nil {
		log.Fatal("Can't parse template")
	}
}

func drawField(w http.ResponseWriter) {
	p := struct {
		FieldSize int
		FieldHtml template.HTML
	}{}
	p.FieldSize = FIELD_SIZE
	p.FieldHtml = template.HTML(strings.Repeat("<div> </div>", FIELD_SIZE*FIELD_SIZE))
	Index.Execute(w, p)
}

func getCell(game *string, x int, y int) (int, byte) {
	if (x >= FIELD_SIZE) || (x < 0) || (y >= FIELD_SIZE) || (y < 0) {
		return -1, '#'
	}
	pos := x + y*FIELD_SIZE
	return pos, (*game)[pos]
}

func iterateCell(game *string, attacker byte, x int, y int) (int, int, bool) {
	pos, cell := getCell(game, x, y)
	switch cell {
	case attacker:
		return pos, 1, false
	case ' ':
		return pos, 0, false
	default:
		return pos, 0, true
	}
}

func iterateLine(game *string, attacker byte, value *GameValue, x int, y int, dx int, dy int, direction int) {
	pos := x + y*FIELD_SIZE
	win := [5]int{}
	for i := 0; i < 5; i++ {
		score := 0
		fail := false
		for j := 0; j < 5; j++ {
			pos, add, fail := iterateCell(game, attacker, x+dx*j, y+dy*j)
			win[j] = pos
			if fail {
				break
			}
			score += add
		}
		if !fail {
			value.field[pos][direction][score]++
			if score == 5 {
				value.winPos = win
			}
		}
		x -= dx
		y -= dy
	}
}

func valuate(game *string, attacker byte, value *GameValue) {
	for x := 0; x < FIELD_SIZE; x++ {
		for y := 0; y < FIELD_SIZE; y++ {
			iterateLine(game, attacker, value, x, y, 0, 1, VERTICAL)
			iterateLine(game, attacker, value, x, y, 1, 0, HORIZONTAL)
			iterateLine(game, attacker, value, x, y, -1, 1, SLASH)
			iterateLine(game, attacker, value, x, y, 1, 1, BACKSLASH)
			if _, cell := getCell(game, x, y); cell != ' ' {
				// Убрать ценность с ячейки, если там что-то уже есть, но останется подсчёт победы
				value.field[x+y*FIELD_SIZE] = [4][6]int{}
				continue
			}
		}
	}
}

func getAnswer(game *string, me byte) (int, [5]int) {
	var enemy byte
	enemy = 'X'
	if me == 'X' {
		enemy = 'O'
	}
	// Определить, что может сделать противник следующим ходом
	value := GameValue{}
	valuate(game, enemy, &value)
	valuate(game, me, &value)
	if value.winPos[2] != 0 {
		// Противник уже победил
		return -1, value.winPos
	}

	cells := [FIELD_SIZE * FIELD_SIZE]int{}
	for pos := 0; pos < FIELD_SIZE*FIELD_SIZE; pos++ {
		for direction := 0; direction < 4; direction++ {
			for length := 1; length < 6; length++ {
				count := float64(value.field[pos][direction][length])
				cells[pos] += int(math.Pow(float64(length), float64(length)) * count)
			}
		}
	}
	m := arrayMax(cells)
	moves := findPositions(cells, m)
	move := moves[rand.Intn(len(moves))]

	// Определить, победил ли кто-нибудь
	value = GameValue{}
	nextGame := replaceAtIndex(*game, me, move)
	valuate(&nextGame, enemy, &value)
	valuate(&nextGame, me, &value)

	return move, value.winPos
}

func appHandler(w http.ResponseWriter, r *http.Request) {
	game := r.FormValue("game")
	if game == "" {
		// No game, draw field
		drawField(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	move, win := getAnswer(&game, 'O')
	if move != -1 {
		game = replaceAtIndex(game, 'O', move)
	}
	json.NewEncoder(w).Encode(struct {
		Game string `json:"game"`
		Move int    `json:"move"`
		Win  [5]int `json:"win"`
	}{game, move, win})
}

func main() {
	rand.Seed(time.Now().Unix())
	loadTemplate()
	http.HandleFunc("/", appHandler)
	http.ListenAndServe(":80", nil)
}
